draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_alpha(1);
draw_set_font(fnt_default);

var xx = 0;
var yy = 0;
if !instance_exists(parent)
then draw_text(xx,yy,"Press 1 to load sprite to use as 'Parent'")
else draw_text(xx,yy,"Press 2 to create a 'Setpiece' and load a sprite for it\nPress Space to generate offset list");
    
draw_set_halign(fa_right);
var cam = view_camera[0]; // view_current doesn't work in Draw GUI
var vx = string(camera_get_view_x(cam));
var vy = string(camera_get_view_y(cam));
var rx = window_get_width();
draw_text(rx,0,"View: "+vx+", "+vy+" (zoom: "+string(zoom)+")");

if instance_exists(parent)
then if instance_exists(inst)
    {
	draw_set_halign(fa_left);
	var mx = device_mouse_x_to_gui(0);
	var my = device_mouse_y_to_gui(0);
	var ix = inst.x;
	var iy = inst.y;
    draw_text(mx,my,"Selected: "+string(inst.name)+"\n"+string(ix)+","+string(iy)+" ("+string(ix - parent.x)+","+string(iy - parent.y)+")");
    }