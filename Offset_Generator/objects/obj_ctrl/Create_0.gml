///@desc INIT
/*
TODO: arrayify parents, childs, sprites

parents can have multiple setpieces

project currently sets one parent and overrides,
\ should support multiple parents

all objects can have at least one sprite that may or may not be unique
\ menu to pick from sprite array OR load new one would be cool

DO NOT COUNT ON GM INDEXING, BETA 2023.8 REPLACES REALS WITH REFS
*/
// Select parent/setpiece functions
inst = noone;
parent = noone;
selected = false;
sprite_indexes = [];
parent_ids = [];
a = 0;

// View control
zoom = 1;
full = false;

// copy/paste function
spr = noone;
name = "";

// Address string stuff
USER_DESKTOP = environment_get_variable("USERPROFILE")+"/Desktop";

// Method Functions
event_user(0);