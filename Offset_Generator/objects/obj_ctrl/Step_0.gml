// SYSTEM INPUTS
if keyboard_check_pressed(vk_escape)
    {
    game_end();
    }
if keyboard_check_pressed(vk_f1)
    {
    var txt = 
@"ESC = Quit
F1 = Show This Window
F2 = Restart
F4 = Fullscreen toggle
F5 = Load Project File
F6 = Save Project File
ARROWS = View Move
SPACE = Generate Offset code and copy it to clipboard
W/S/A/D = View Move (while nothing is selected)
Left CTRL + W/S/A/D = Single Pixel View Move (while nothing is selected)
'1' = Create parent (only valid if no parent is already present)
9/0 (or middle mouse wheel) = Zoom In/Out

Left Mouse (off parent/setpiece) = Select 
Left Mouse (on parent/setpiece) = Mouse move 
Right Mouse = De-select all setpieces
Shift + Right Mouse = De-select single setpiece
Shift + Left Mouse = Select Multiple setpieces

While a setpiece is selected:
CTRL + Arrows = 1 pixel move per button press
CTRL + C/V = Copy/Paste setpiece
ENTER = rename setpiece (will appear as given name in generated code)
DEL = Delete setpiece";
    fullscreen_kludge();
    show_message(txt);
    }
if keyboard_check_pressed(vk_f2)
    {
    game_restart();
    }
if keyboard_check_pressed(vk_f4)
    {
    window_set_fullscreen(!window_get_fullscreen());
    }
    
// Load/Save Project File (GM blob, wouldn't recommend for games but real convenient here)
if keyboard_check_pressed(vk_f5) then project_load();
if keyboard_check_pressed(vk_f6) then project_save();

// VIEW CONTROL
if !selected
    {
	// VIEW CONTROL
	var cam = view_camera[view_current];
	if keyboard_check(ord("W")) then y -= 2;
	if keyboard_check(ord("S")) then y += 2;
	if keyboard_check(ord("A")) then x -= 2;
	if keyboard_check(ord("D")) then x += 2;
	camera_set_view_pos(cam,x,y);
    }
    
// Keyboard Zoom
if keyboard_check_pressed(ord("9")) then zoom_in();
if keyboard_check_pressed(ord("0")) then zoom_out();
    
// Mouse Zoom
if mouse_wheel_up() then zoom_in();
if mouse_wheel_down() then zoom_out();
    
// UI KEYBOARD EVENTS
// Create Parent
if keyboard_check_pressed(ord("1"))
    {
    parent_create(mouse_x,mouse_y);
    }
    
// Create Setpiece
if keyboard_check_pressed(ord("2"))
    {
    setpiece_create(mouse_x,mouse_y);
    }
    
// Generate Offset Code
if keyboard_check_pressed(vk_space) then generate_code();
    
// Rename an object
if keyboard_check_pressed(vk_enter) then rename_instance();
    
// Copy/Paste Setpieces
// (Note: Only works with last selected, not multiple)
// (Note: Don't need more than one parent, so pastes are always setpieces)
if keyboard_check(vk_control)
    {
    if keyboard_check_pressed(ord("C")) && inst != noone
        {
        spr = inst.spr;
        name = inst.name;
        }
    if keyboard_check_pressed(ord("V")) && inst == noone && spr != noone
        {
        var mx = mouse_x;
        var my = mouse_y;
        var setpiece;
        setpiece = instance_create_depth(mx,my,0,obj_setpiece);
        setpiece.spr = spr;
        setpiece.name = name;
        }
    }
    
// MOUSE EVENTS
// Select Parent/Setpiece
if device_mouse_check_button_pressed(0,mb_left)
    {
    if !keyboard_check(vk_shift)
        {
        with obj_parent selected = false;
        selected = false;
        }
    inst = instance_position(mouse_x,mouse_y,obj_setpiece);
    if inst != noone
        {
        selected = true;
        inst.selected = true;
        }
    else 
        {
        if instance_position(mouse_x,mouse_y,parent) != noone
            {
            selected = true;
            parent.selected = true;
            inst = parent;
            }
        }
    }
    
// De-Select Parent/Setpiece
if device_mouse_check_button_pressed(0,mb_right)
    {
    if keyboard_check(vk_shift) // De-Select One
        {
        inst = instance_position(mouse_x,mouse_y,obj_setpiece);
        if inst != noone
            {
            inst.selected = false;
            inst = noone;
            }
        }
    else // De-Select All
        {
        selected = false;
        with inst selected = false;
        inst = noone;
        with obj_parent selected = false;
        }
    }
    
// Alpha pulse for child objects
if a == 0
    {
    image_alpha -= 0.025;
    if image_alpha < 0.025 then a = 1;
    }
else
    {
    image_alpha += 0.025;
    if image_alpha >= 0.5 then a = 0;
    }