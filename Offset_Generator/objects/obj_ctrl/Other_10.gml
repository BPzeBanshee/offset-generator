/// @desc Method Functions

// Hack to drop fullscreen when opening window
// due to GM messing with window priority when fullscreen
fullscreen_kludge = function(){
if window_get_fullscreen()
    {
	full = true;
    window_set_fullscreen(false);
    }
}

// Camera functions
zoom_in = function(){
var cam = view_camera[view_current];
var cam_w = camera_get_view_width(cam);
var cam_h = camera_get_view_height(cam);
var cx = camera_get_view_x(cam) + (cam_w/4);
var cy = camera_get_view_y(cam) + (cam_h/4);
camera_set_view_size(cam,cam_w/2,cam_h/2);
camera_set_view_pos(cam,cx,cy);
zoom += 1;
}
zoom_out = function(){
if zoom <= 1 exit;
var cam = view_camera[view_current];
var cam_w = camera_get_view_width(cam);
var cam_h = camera_get_view_height(cam);
var cx = camera_get_view_x(cam) - (cam_w/2);
var cy = camera_get_view_y(cam) - (cam_h/2);
camera_set_view_size(cam,cam_w*2,cam_h*2);
camera_set_view_pos(cam,cx,cy);
zoom -= 1;
}

// Load Project File
project_load = function(){
fullscreen_kludge();
var g = get_open_filename_ext("*.zip","project.zip",USER_DESKTOP,"Load project placement file...");
if g != "" then scr_project_load(g);
alarm[0] = 10;
}

// Save Project File
project_save = function(){
fullscreen_kludge();
var g = get_save_filename_ext("*.zip","project.zip",USER_DESKTOP,"Save project placement file...");
if g != "" then scr_project_save(g);
alarm[0] = 10;
}

// Create New Parent / Delete Parent
parent_create = function(_x,_y){
fullscreen_kludge();
if parent == noone
    {
    var spr_dir = get_open_filename("*.png","");
    if spr_dir != ""
        {
		var spr = sprite_add(spr_dir,0,false,false,0,0);
		if !sprite_exists(spr) then exit;
		array_push(sprite_indexes,spr);
		
        parent = instance_create_depth(_x,_y,2,obj_parent);
        parent.name = string_replace(string(parent.id),"ref ","")+"_"+strip_filename(spr_dir);
		parent.spr = spr;
		parent.spr_id = array_length(sprite_indexes)-1;
		parent.update_sprite();
		array_push(parent_ids,parent);
        }
    }
else
    {
    var q = show_question("Parent already exists. Destroy?");
    if q == true
        {
        with parent instance_destroy();
        parent = noone;
        }
    }
alarm[0] = 10;
}

// Create Setpiece
setpiece_create = function(_x,_y){
fullscreen_kludge();
var spr_dir = get_open_filename(".png","");
if spr_dir != ""
    {
	var spr = sprite_add(spr_dir,0,false,false,0,0);
	if !sprite_exists(spr) then exit;
	array_push(sprite_indexes,spr);
	
    var setpiece = instance_create_depth(_x,_y,1,obj_setpiece);
    setpiece.spr = spr;
	setpiece.spr_id = array_length(sprite_indexes)-1;
    setpiece.name = string_replace(string(setpiece.id),"ref ","")+"_"+strip_filename(spr_dir);
	setpiece.update_sprite();
	if parent != noone
		{
		setpiece.parent = parent;
		setpiece.par_id = array_length(parent_ids)-1;
		array_push(parent.setpieces,setpiece);
		}
    }
alarm[0] = 10;
}

// Generate Offset Code
generate_code = function(){
/// @description  Generate offsets list and copy to clipboard
var str = "";
for (var i=0;i<array_length(parent_ids);i++)
	{
	var o = parent_ids[i];
	for (var j=0; j<array_length(o.setpieces);j++)
		{
		// pull x/y values from children
		var xx = o.setpieces[j].x - o.x;
		var yy = o.setpieces[j].y - o.y;
		
		// work out +/- strings
		var str_op = " + ";
		var str_op2 = " + ";
		if xx < 0
			{
			str_op = " - ";
			xx = xx * -1;
			}
		if yy < 0
			{
			str_op2 = " - ";
			yy = yy * -1;
			}
			
		// null if value 0
		var fx = xx==0 ? "" : str_op+string(xx);
		var fy = yy==0 ? "" : str_op2+string(yy);
		
		// finally, add to string
		var setname = string(o.setpieces[j].name);
		var parname = string(o.name);
		str += setname+".x = "+parname+".x"+fx+";\n";
        str += setname+".y = "+parname+".y"+fy+";\n";
		}
	}
clipboard_set_text(str);
	
// OLD CODE
/*if instance_exists(parent) 
then with parent
    {
    var xx,yy,str_op,str_op2,fx,fy;
    var str = "";
    for (var i=0;i<instance_count;i++)
        {
        // Only proceed if setpiece
        if instance_id[i].object_index == obj_setpiece
            {
            // Grab relative offsets
            xx[i] = instance_id[i].x - x;
            yy[i] = instance_id[i].y - y;
            
            // Add - operator to string and invert if negative
            if xx[i] < 0 
                {
                str_op = " - "; 
                xx[i] = xx[i] * -1;
                }
            else str_op = " + ";
            if yy[i] < 0
                {
                str_op2 = " - ";
                yy[i] = yy[i] * -1;
                }
            else str_op2 = " + ";
            
            // Generate final x/y addition strings
			// tern = statement ? true : false
			//fx = xx[i]==0 ? "" : str_op+string(xx[i]);
            if xx[i] == 0 {fx = "";} else fx = str_op+string(xx[i]);
			//fy = yy[i]==0 ? "" : str_op2+string(yy[i]);
            if yy[i] == 0 {fy = "";} else fy = str_op2+string(yy[i]);
            
            // Finally, generate the string
            //string(instance_id[i].id)+"_"+
            str += string(instance_id[i].name)+".x = x"+fx+";\n";
            str += string(instance_id[i].name)+".y = y"+fy+";\n";
            }
        }
    
    clipboard_set_text(str);
	}*/
fullscreen_kludge();
show_message("Code copied to clipboard!");
alarm[0] = 10;
}

//'Rename' an object for text generation purposes
rename_instance = function(){
if inst == noone then exit;
fullscreen_kludge();
var s = get_string("Enter new name for setpiece",inst.name);
if s != "" then inst.name = s;
alarm[0] = 10;
}