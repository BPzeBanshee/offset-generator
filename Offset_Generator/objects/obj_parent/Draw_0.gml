if !sprite_exists(sprite_index) then exit;
draw_self();
if selected
    {
    gpu_set_blendmode_ext(bm_src_alpha,bm_dest_alpha);
    draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,obj_ctrl.image_alpha);
    gpu_set_blendmode(bm_normal);
    }