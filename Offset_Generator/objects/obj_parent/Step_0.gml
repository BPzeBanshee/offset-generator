if selected == true
    {    
    // Keyboard shift
    if keyboard_check(vk_lcontrol)
        {
        if keyboard_check_pressed(vk_left) then x -= 1;
        if keyboard_check_pressed(vk_right) then x += 1;
        if keyboard_check_pressed(vk_up) then y -= 1;
        if keyboard_check_pressed(vk_down) then y += 1;
        }
    else
        {
        if keyboard_check(vk_left) then x -= 1;
        if keyboard_check(vk_right) then x += 1;
        if keyboard_check(vk_up) then y -= 1;
        if keyboard_check(vk_down) then y += 1;
        }
    
    // Mouse move
    if device_mouse_check_button(0,mb_left)
        {
        if xx == 0 && yy == 0
            {
            xx = mouse_x - x;
            yy = mouse_y - y;
            }
        x = mouse_x - xx;
        y = mouse_y - yy;
        }
    else
        {
        xx = 0;
        yy = 0;
        }
        
    if device_mouse_check_button(0,mb_right)
        {
        selected = false;
        image_blend = c_white;
        }
        
    if keyboard_check_pressed(vk_delete) then instance_destroy();
    }