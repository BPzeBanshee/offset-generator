/// @description strip_filename(string)
/// @param string
var s;
s = argument0;

// Absolutely gross but it seems to work on Windows
/*if string_count("\",s) > 0
then do s = string_delete(s,1,1) 
until string_count("\",s) == 0;*/

// Alternatively try with backwards strings if it has any
/*if string_count("/",s) > 0
then do s = string_delete(s,1,1) 
until string_count("/",s) == 0;*/

// It's 2023 and I now recall that filename_name is a thing
s = filename_name(s);

// Remove the trailing file extension
s = string_replace(s,".png","");

// Done!
return s;
