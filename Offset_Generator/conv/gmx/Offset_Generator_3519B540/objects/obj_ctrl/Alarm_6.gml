/// @description  'Rename' an object for text generation purposes
if inst == noone then exit;
event_user(0);
var s;
s = get_string("Enter new name for setpiece",inst.name);
if s != "" then inst.name = s;

