/// @description  Generate offsets list and copy to clipboard
if instance_exists(parent) 
then with parent
    {
    var str,xx,yy,str_op,str_op2,fx,fy;
    str = "";
    for (i=0;i<instance_count;i++)
        {
        // Only proceed if setpiece
        if instance_id_get( i ).object_index == obj_setpiece
            {
            // Grab relative offsets
            xx[i] = instance_id_get( i ).x - x;
            yy[i] = instance_id_get( i ).y - y;
            
            // Add - operator to string and invert if negative
            if xx[i] < 0 
                {
                str_op = " - "; 
                xx[i] = xx[i] * -1;
                }
            else str_op = " + ";
            if yy[i] < 0
                {
                str_op2 = " - ";
                yy[i] = yy[i] * -1;
                }
            else str_op2 = " + ";
            
            // Generate final x/y addition strings
            if xx[i] == 0 {fx = "";}
            else fx = str_op+string(xx[i]);
            if yy[i] == 0 {fy = "";}
            else fy = str_op2+string(yy[i]);
            
            // Finally, generate the string
            //string(instance_id[i].id)+"_"+
            str += string(instance_id_get( i ).name)+".x = x"+fx+";"+chr(10);
            str += string(instance_id_get( i ).name)+".y = y"+fy+";"+chr(10);
            }
        }
    
    clipboard_set_text(str);
    event_user(0);
    show_message("Code copied to clipboard!");
    }

