/// @description  INIT

// Select parent/setpiece functions
inst = noone;
parent = noone;
selected = false;
a = 0;

// View control
//window_set_size(display_get_width()*0.75,display_get_height()*0.75);
zoom = 1;

// copy/paste function
spr = noone;
name = "";

// mouse place function (updates on click to avoid problems when switching fullscreen)
mx = mouse_x;
my = mouse_y;

