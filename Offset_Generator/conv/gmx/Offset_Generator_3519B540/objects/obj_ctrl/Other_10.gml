/// @description  Hack to drop fullscreen when opening window
// due to GMS v1.4 messing with window priority when fullscreen
if window_get_fullscreen()
    {
    window_set_fullscreen(false);
    alarm[0] = 5;
    }

