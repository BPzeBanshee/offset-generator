draw_set_color(c_white);
draw_set_halign(fa_left);
draw_set_alpha(1);
draw_set_font(fnt_default);
var xx,yy,rx,ry;
xx = 0;
yy = 0;
rx = __view_get( e__VW.WPort, 0 );
ry = __view_get( e__VW.HPort, 0 );
if !instance_exists(parent)
    {
    draw_text(xx,yy,string_hash_to_newline("Press 1 to load sprite to use as 'Parent'"));
    }
else
    {
    draw_text(xx,yy,string_hash_to_newline("Press 2 to create a 'Setpiece' and load a sprite for it\\nPress Space to generate offset list"));
    }
    
draw_set_halign(fa_right);
var vx,vy;
vx = string(__view_get( e__VW.XView, 0 ));
vy = string(__view_get( e__VW.YView, 0 ));
draw_text(rx,0,string_hash_to_newline("View: "+vx+", "+vy+" (zoom: "+string(zoom)+")"));

if instance_exists(parent)
then if instance_exists(inst)
    {
    draw_text(rx,10,string_hash_to_newline("Inst: "+string(inst.name)+"\\n("+string(inst.x - parent.x)+","+string(inst.y - parent.y)+")"));
    }

