/// @description  Create New Parent / Delete Parent
if !instance_exists(parent)
    {
    var spr_dir;
    spr_dir = get_open_filename(".png","");
    if spr_dir != ""
        {
        parent = instance_create(mx,my,obj_parent);
        parent.spr = sprite_add(spr_dir,0,false,false,0,0);
        parent.name = string(parent.id)+"_"+strip_filename(spr_dir);
        }
    }
else
    {
    var q;
    q = show_question("Parent already exists. Destroy?");
    if q == true
        {
        with parent instance_destroy();
        parent = noone;
        }
    else exit;
    }

