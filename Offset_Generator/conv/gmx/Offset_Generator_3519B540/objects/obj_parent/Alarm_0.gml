/// @description  Set sprite offset/selection mask
sprite_set_offset(spr,sprite_get_width(spr)/2,sprite_get_height(spr)/2);
sprite_collision_mask(spr, false, 0, 0, 0, 0, 0, 0, 0);
sprite_index = spr;
mask_index = spr;

