///@desc scr_project_load(dir)
///@param {string} dir
///@returns {real} 0
function scr_project_load(dir){
// file handle
if !file_exists(dir) then return -1;
var myzip = zip_unzip(dir,working_directory);
if myzip < 1 then return -2;

// json
var s = "";
var f = file_text_open_read("project.json");
while !file_text_eof(f) s += file_text_readln(f);
file_text_close(f);
var myjson = json_parse(s);
if !is_struct(myjson) then return -3;

// clear all resources first
with obj_setpiece instance_destroy();
with obj_parent instance_destroy();
for (var i=0;i<array_length(sprite_indexes);i++)
	{
	sprite_delete(sprite_indexes[i]);
	}
sprite_indexes = [];
parent_ids = [];

// read and generate sprite array
// no subimages so we just save the number
if variable_struct_exists(myjson,"sprite_map")
	{
	var mymap = myjson[$ "sprite_map"];
	for (var i=0; i<mymap; i++)
		{
		sprite_indexes[i] = sprite_add(string(i)+".png",0,false,false,0,0);
		}
	}

// read and create parents
if variable_struct_exists(myjson,"parent_map")
	{
	var mymap = myjson[$ "parent_map"];
	var m,o;
	for (var i=0;i<variable_struct_names_count(mymap); i++)
		{
		m = mymap[$ string(i)];
		o = instance_create_depth(m[$ "x"],m[$ "y"],2,obj_parent);
		o.sprite_index = sprite_indexes[m[$ "sprite_id"]];
		parent_ids[i] = o.id;
		}
	}
	
// read and create setpieces
if variable_struct_exists(myjson,"setpiece_map")
	{
	var mymap = myjson[$ "setpiece_map"];
	var m,o;
	for (var i=0;i<variable_struct_names_count(mymap); i++)
		{
		m = mymap[$ string(i)];
		o = instance_create_depth(m[$ "x"],m[$ "y"],1,obj_setpiece);
		o.parent = parent_ids[m[$ "parent_id"]];
		o.spr_id = m[$ "sprite_id"];
		o.sprite_index = sprite_indexes[o.spr_id];
		}
	}
	
// get view settings
if variable_struct_exists(myjson,"view_map")
	{
	var viewmap = variable_struct_get(myjson,"view_map");
	x = viewmap[$ "x"];
	y = viewmap[$ "y"];
	camera_set_view_pos(view_camera[0],x,y);
	zoom = viewmap[$ "zoom"];
	}
	
// select parent
parent = array_last(parent_ids);
	
// clear memory
delete mymap;
delete myjson;
}

///@desc scr_project_save(dir)
///@param {string} dir
///@returns {real} 0
function scr_project_save(dir){
var myjson = {};

// dump all sprites to working_directory
for (var i=0;i<array_length(sprite_indexes);i++)
	{
	sprite_save(sprite_indexes[i],0,string(i)+".png");
	}
	
// save index count (we dont need to store anything else here)
variable_struct_set(myjson,"sprite_map",array_length(sprite_indexes));

// generate parent instance map
var parentmap = {};
var m = {};
for (var i=0;i<array_length(parent_ids);i++)
	{
	m[$ "x"] = parent_ids[i].x;
	m[$ "y"] = parent_ids[i].y;
	m[$ "name"] = parent_ids[i].name;
	m[$ "sprite_id"] = parent_ids[i].spr_id;
	variable_struct_set(parentmap,string(i),m);
	}
variable_struct_set(myjson,"parent_map",parentmap);

// generate setpiece instance map (TBA)
var setpiecemap = {};
var s = {};
var o;
for (var i=0;i<instance_number(obj_setpiece);i++)
	{
	o = instance_find(obj_setpiece,i);
	s[$ "x"] = o.x;
	s[$ "y"] = o.y;
	s[$ "name"] = o.name;
	s[$ "parent_id"] = o.par_id;
	s[$ "sprite_id"] = o.spr_id;
	variable_struct_set(setpiecemap,string(i),s);
	}
variable_struct_set(myjson,"setpiece_map",setpiecemap);

// get view settings
var viewmap = {};
viewmap[$ "x"] = x;
viewmap[$ "y"] = y;
viewmap[$ "zoom"] = zoom;
variable_struct_set(myjson,"view_map",viewmap);

// add to zip
var myzip = zip_create();
for (var i=0;i<array_length(sprite_indexes);i++)
	{
	zip_add_file(myzip,string(i)+".png",string(i)+".png");
	}
var f = file_text_open_write("project.json");
file_text_write_string(f,json_stringify(myjson,true));
file_text_close(f);
zip_add_file(myzip,"project.json","project.json");
zip_save(myzip,dir);

delete parentmap;
delete setpiecemap;
delete myjson;
}